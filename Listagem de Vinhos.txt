Nome: Papa Figos
Produtor: Casa Ferreirinha
Região: Douro
Tipo: Tinto
Ano: 2017
Castas:
Harmonização: Vaca, Massa, Cordeiro e Veado
Crítica:Um vinho elegante, com aromas de frutas vermelhas maduras, notas florais e de cacau. Taninos finos e corpo médio proveniente do estágio de oito meses em barrica, que também se faz perceber no paladar através de algumas notas especiadas.

Nome: Castelo do Sulco - Seleção dos Enólogos
Produtor: Quinta do Gradil
Região: Lisboa
Tipo: Tinto
Ano: 2018
Castas: Aragonez Syrah
Harmonização: Carne de Vaca, Massa, Cordeiro e Carne de Caça
Crítica: Tem muito boa cor e perfil jovem e vinoso, com notas vegetais de ervas a acompanharem a fruta, amoras e framboesas, um toque floral. Na boca tem corpo mais do que suficiente para envolver os taninos, num conjunto bastante equilibrado e refrescante, sempre com a fruta em primeiro plano.

Nome: Martha's - Colheita
Produtor: Martha's Wines & Spirits
Região: Douro
Tipo: Tinto
Ano: 2015
Castas:
Harmonização: Carne de Vaca, Massa, Cordeiro e Carne de Caça
Crítica: É um vinho elegante, com texturas complexas e sabores excelentes. Apresenta uma cor vermelha alegre, com aromas frutados e frescos. O paladar é requintado e suave, com sabores tostados com um final persistente.

Nome: Aluzé
Produtor: Pessegueiro
Região: Douro
Tipo: Tinto 
Ano: 2015
Castas:
Harmonização: Carne de Vaca, Massa, Cordeiro e Carne de Caça
Crítica: Destaca-se pelos seus maduros aromas de frutas vermelhas, como cerejas e groselhas mescladas a toques de alecrim, cravo e pimenta branca.

Nome: Herdade de São Miguel - Colheita Seleccionada
Produtor: Herdade de São Miguel
Região: Alentejo
Tipo: Tinto
Ano: 2020
Castas: Syrah, Cabernet Sauvignon, Alicante Bouschet
Harmonização: Harmonização: Carne de Vaca, Massa, Cordeiro, Vitela e Aves
Crítica: Taninos redondos elegantes, toque de pimenta ligeiro, frutos negros bem presentes. Boa acidez.

Nome: Santos da Casa Reserva
Produtor: Santos da Casa
Região: Alentejo
Tipo: Tinto
Ano: 2015
Castas: Syrah, Cabernet Sauvignon e Alicante Bouschet
Harmonização: Carne de Vaca, Massa, Vitela e Aves
Crítica: Vinho de corpo intenso, porem macio e redondo. Bem típico da região do Alentejo. Vinho para ter sempre na adega.

Nome: Duas Quintas
Produtor: Ramos Pinto
Região: Douro
Tipo: Tinto
Ano: 2014
Castas: Tempranillo
Harmonização: Carne de Vaca, Massa, Cordeiro e Carne de Caça
Crítica: Um clássico português. Notas de framboesa e chocolate, estala seco e sem a chata da madeira que por vezes abafa a maioria dos vinhos. Precisa respirar.

Nome: Duas Quintas
Produtor: Ramos Pinto
Região: Douro
Tipo: Branco
Ano: 2019
Castas: Rabigato, Viosino
Harmonização: Marisco, Peixes Magros, Carne Curada, Aperitivos e Lanches.
Crítica: Aroma intenso e bem atraente a maçã, limão, flores e um toque de azeite. Na boca é leve, bem subtil e com ligeira untuosidade. Bom final de boca de média duração.

Nome: Coutada Velha
Produtor: Coutada Velha
Região: Alentejo
Tipo: Branco
Ano: 2020
Castas: 
Harmonização: Massa, Carne de Porco, Marisco e Peixes Magros.
Crítica: Notas florais, equilibrado e boas pernas. Intenso e claro.

Nome: Casal Garcia
Produtor: Casal Garcia
Região: Minho
Tipo: Verde Branco
Ano: 2020
Castas: 
Harmonização: Marisco, Vegetariano, Aperitivos e lanches.
Crítica: Límpido, amarelo. No nariz, toques cítricos suaves, maçã verde e pêra. Ótimo vinho!

Nome: Ponte de Lima Vinhão
Produtor: Adega Cooperativa De Ponte De Lima
Região: Douro
Tipo: Verde Tinto
Ano: 2019
Castas: 
Harmonização: Marisco, Vegetariano, Aperitivos e lanches.
Crítica: Aroma muito agradável, com notas de ananás e maçã. Bastante acidez que combina perfeitamente com queijo. Na boca, bem leve e fresco.

Nome: Quinta do Cardo Reserva Caladoc
Produtor: Quinta do Cardo
Região: Beira Interior
Tipo: Rosé
Ano: 2016
Castas: Malbec
Harmonização: Marisco, Vegetariano, Aperitivos e lanches.
Crítica: Presença de cereja, ginja, maçã assad. Surge bem integrada a madeira. Evolução interessante em copo!

Nome: Barros Colheita
Produtor: Barros
Região: Porto
Tipo: Porto
Ano: 2016
Castas: 
Harmonização: Aperitivos, Queijos e Lanches
Crítica: Bastante aceitável. Alguma fruta. Nota de canela e caramelo, açucar queimado ligeiro.

Nome: Czar Grand Cuvée Rosé Brut
Produtor: Murganheira
Região: Beiras
Tipo: Espumante
Ano: 2006
Castas: Pinot Noir
Harmonização: Mariscos, Peixes Magros e Aperitivos.
Crítica: Espumante nacional de qualidade. A prova de que em Portugal temos espumantes capazes de competir com os melhores do mundo!

Nome: Muralhas
Produtor: Adega de Monção
Região: Minho
Tipo: Verde
Ano: 2019
Castas: 
Harmonização: Marisco, Vegetariano, Aperitivos e Lanches
Crítica: Este vinho verde é já um clássico, apresenta uma cor ligeiramente esverdeado. É frutado e tem uma boa acidez. É sempre muito bom bebê-lo muito fresco.
