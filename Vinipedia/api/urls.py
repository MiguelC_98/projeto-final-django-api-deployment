from django.urls import path
from . import views

urlpatterns = [
    path('', views.APIRoot.as_view(), name=views.APIRoot.name),
    path('wines/', views.WineList.as_view(), name=views.WineList.name),
    path('wines/<int:pk>', views.WineDetail.as_view(), name=views.WineDetail.name),
    path('wines/<int:pk>/evaluations', views.evaluations_per_wine, name='wineevaluations-list'),
    path('evaluation/<int:pk>', views.EvaluationDetail.as_view(), name=views.EvaluationDetail.name),
    path('producers/', views.ProducerList.as_view(), name=views.ProducerList.name),
    path('producers/<int:pk>', views.ProducerDetail.as_view(), name=views.ProducerDetail.name),
    path('producers/<int:pk>/pictures', views.producer_images, name='imagesforproducer-list'),
    path('grapes/', views.GrapesList.as_view(), name=views.GrapesList.name),
    path('grapes/<int:pk>', views.GrapeDetail.as_view(), name=views.GrapeDetail.name)
]