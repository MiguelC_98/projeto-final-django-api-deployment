from django.shortcuts import render
from rest_framework import generics, status, filters
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticatedOrReadOnly, SAFE_METHODS
from rest_framework.response import Response
from rest_framework.reverse import reverse
from users.models import Profile
from wines.models import Wine, Evaluation
from grapes.models import Grape
from producers.models import Producer, ProducerPicture, Address
from . import serializers
from api.custompermissions import IsAdminOrReadOnly


class WineList(generics.ListCreateAPIView):
    queryset = Wine.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)
    name = 'wine-list'
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = (
        '^name',
        '^year',
        'type',
        'region'
    )
    ordering_fields = (
        'name',
    )

    def get_serializer_class(self):
        if self.request.method in SAFE_METHODS:
            return serializers.WineSerializer
        elif self.request.method == 'POST':
            return serializers.BasicWineSerializer


class WineDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Wine.objects.all()
    serializer_class = serializers.WineSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)
    name = 'wine-detail'

    def get_serializer_class(self):
        if self.request.method in SAFE_METHODS:
            return serializers.WineSerializer
        elif self.request.method in ('PATCH', 'PUT', 'DELETE'):
            return serializers.BasicWineSerializer


class ProducerList(generics.ListCreateAPIView):
    queryset = Producer.objects.all()
    serializer_class = serializers.ProducerSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    name = 'producer-list'
    search_fields = (
        '^name',
    )
    ordering_fields = (
        'name',
    )


class ProducerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Producer.objects.all()
    serializer_class = serializers.ProducerSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)
    name = 'producer-detail'


@api_view(['GET'])
def producer_images(request, pk):
    producer = get_object_or_404(Producer, pk=pk)
    if request.method == 'GET':
        pictures = ProducerPicture.objects.filter(producer=producer)
        serializer = serializers.ProducerPicturesSerializer(pictures, many=True, context={'request': request})
        return Response(serializer.data)


class GrapesList(generics.ListCreateAPIView):
    queryset = Grape.objects.all()
    serializer_class = serializers.GrapesSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    name = 'grapes-list'
    search_fields = (
        '^name',
    )
    ordering_fields = (
        'name',
    )


class GrapeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Grape.objects.all()
    serializer_class = serializers.GrapesSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)
    name = 'grape-detail'


class EvaluationDetail(generics.RetrieveAPIView):
    queryset = Evaluation.objects.all()
    serializer_class = serializers.EvaluationSerializer
    name = 'evaluation-detail'


@api_view(['GET', 'POST'])
def evaluations_per_wine(request, pk):
    wine = get_object_or_404(Wine, pk=pk)
    if request.method == 'GET':
        evaluations = Evaluation.objects.filter(wine=wine)
        serializer = serializers.EvaluationSerializer(evaluations, many=True, context={'request': request})
        return Response(serializer.data)
    elif request.method == 'POST':
        if request.user.is_authenticated:
            request.data['user'] = request.user.id
            request.data['wine'] = pk
            serializer = serializers.EvaluationPostSerializer(data=request.data, context={'request': request})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class APIRoot(generics.GenericAPIView):
    name = 'api-root'

    def get(self, request, *args, **kwargs):
        return Response({
            'wines': reverse(WineList.name, request=request),
            'grapes': reverse(GrapesList.name, request=request),
            'producers': reverse(ProducerList.name, request=request),
        })
