from rest_framework import serializers
from users.models import Profile
from wines.models import Wine, Evaluation
from grapes.models import Grape
from producers.models import Producer, ProducerPicture, Address
from django.contrib.auth.models import User


class ProfilePictureSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'picture',
        )


class UserForEvSerializer(serializers.HyperlinkedModelSerializer):
    profile = ProfilePictureSerializer()

    class Meta:
        model = User
        fields = (
            'username',
            'profile'
        )


class EvaluationSerializer(serializers.HyperlinkedModelSerializer):
    user = UserForEvSerializer()

    class Meta:
        model = Evaluation
        fields = (
            'user',
            'wine',
            'description',
            'score',
            'created'
        )


class AddressSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Address
        fields = (
            'street_name',
            'number',
            'postal_code',
            'location'
        )


class ProducerPicturesSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = ProducerPicture
        fields = (
            'picture',
        )


class ProducerSerializer(serializers.HyperlinkedModelSerializer):

    pictures = ProducerPicturesSerializer(many=True)
    address = AddressSerializer()

    class Meta:
        model = Producer
        fields = (
            'url',
            'name',
            'description',
            'address',
            'website',
            'pictures'
        )


class GrapesSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Grape
        fields = (
            'name',
            'description',
            'picture'
        )


class WineSerializer(serializers.ModelSerializer):

    grapes = GrapesSerializer(many=True)
    evaluations = EvaluationSerializer(many=True)
    producer = ProducerSerializer()

    class Meta:
        model = Wine
        fields = (
            'url',
            'name',
            'description',
            'producer',
            'type',
            'year',
            'score',
            'region',
            'alcohol_percentage',
            'evaluations',
            'grapes',
            'picture'
        )


class BasicWineSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Wine
        fields = (
            'name',
            'description',
            'producer',
            'type',
            'year',
            'region',
            'alcohol_percentage',
            'grapes',
            'picture'
        )


class EvaluationPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Evaluation
        fields = (
            'user',
            'wine',
            'description',
            'score'
        )

