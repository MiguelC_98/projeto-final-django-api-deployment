from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404, redirect
from wines.models import Wine, Evaluation
from .forms import EvaluationForm
import random
from collections import Counter


def wine_search(request):
    if request.method == 'GET':
        query = request.GET.get("query")
        if query.isnumeric():
            results_without_page = Wine.objects.filter(name__contains=query) | Wine.objects.filter(year=query)
        else:
            results_without_page = Wine.objects.filter(name__contains=query)
        paginator = Paginator(results_without_page, 3)
        page = request.GET.get('page')
        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage:
            results = paginator.page(paginator.num_pages)

        return render(request, 'wines/wines/search.html', {'results': results, 'query': query, 'page':page})


def wine_list(request, wine_type=None):

    if wine_type:
        object_list = Wine.objects.filter(type=wine_type)
    else:
        object_list = Wine.objects.all()
    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')

    try:
        wines = paginator.page(page)
    except PageNotAnInteger:
        wines = paginator.page(1)
    except EmptyPage:
        wines = paginator.page(paginator.num_pages)

    return render(request, "wines/wines/list.html", {'wines': wines, 'page': page, 'wine_type': wine_type})


def wine_detail(request, pk):
    wine = get_object_or_404(Wine, pk=pk)
    evaluations = Evaluation.objects.filter(wine=wine)
    evaluation_form = None
    new_ev = None
    if request.method == 'POST':
        evaluation_form = EvaluationForm(data=request.POST)
        if evaluation_form.is_valid():
            new_ev = evaluation_form.save(commit=False)
            new_ev.wine = wine
            new_ev.user_id = request.user.id
            new_ev.save()
            return redirect(wine)
    elif request.method == 'GET':
        evaluation_form = EvaluationForm()
        wine.visits = wine.visits + 1
        wine.save()
        recent_wines = list(request.session.get('recent_wines', []))
        if wine.id in recent_wines:
            i = recent_wines.index(wine.id)
            recent_wines.pop(i)
        recent_wines.insert(0, wine.id)
        if len(recent_wines) > 5:
            recent_wines.pop()
        request.session['recent_wines'] = recent_wines
    return render(request, 'wines/wines/detail.html',
                  {'wine': wine, 'evaluations': evaluations, 'evaluation_form': evaluation_form, 'new_ev': new_ev})


def homepage(request):
    wines = list(Wine.objects.all())
    if len(wines) > 5:
        random_wines = random.sample(wines, 5)
    else:
        random_wines = wines.copy()
    recent_wines_sess = request.session.get('recent_wines', [])
    recent_wines = []
    """wines_dict = {}"""
    for wine in wines:
        """wines_dict[str(wine.id)] = wine.visits"""
        if len(recent_wines_sess) > 0:
            if wine.id in recent_wines_sess:
                index = recent_wines_sess.index(wine.id)
                recent_wines.insert(index, wine) #a ordenar pelos mais recentemente visitados primeiro
    """most_visited_wines_counter = Counter(wines_dict).most_common(5)
    most_visited_wines = []
    for pkish in most_visited_wines_counter:
        pk = int(pkish[0])
        visited_wine = get_object_or_404(Wine, pk=pk)
        most_visited_wines.append(visited_wine)""" # gosto de complicar coisas :)
    most_visited_wines = Wine.objects.all().order_by('-visits')[:5]
    return render(request, 'homepage.html',
                  {'random_wines': random_wines,
                   'recent_wines': recent_wines,
                   'most_visited_wines': most_visited_wines})
