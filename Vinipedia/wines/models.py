from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
import datetime

from django.db.models import Avg
from django.urls import reverse
from grapes.models import Grape
from producers.models import Producer


def wine_directory_path(instance, filename):
    return 'wines/{0}/{1}'.format(instance.name, filename)


class Wine(models.Model):
    YEAR_CHOICES = []
    for r in range(1780, (datetime.datetime.now().year + 1)):
        YEAR_CHOICES.append((r, r))

    TYPE_CHOICES = (
        ('tinto', 'Tinto'),
        ('branco', 'Branco'),
        ('verde', 'Verde'),
        ('verde branco', 'Verde Branco'),
        ('verde tinto', 'Verde Tinto'),
        ('do Porto', 'Do Porto'),
        ('rosé', 'Rosé'),
        ('espumante', 'Espumante')
    )

    name = models.CharField(max_length=100)
    description = models.TextField()
    producer = models.ForeignKey(Producer, on_delete=models.CASCADE, related_name='wines')
    type = models.CharField(choices=TYPE_CHOICES, max_length=25)
    year = models.IntegerField(choices=YEAR_CHOICES, default=datetime.datetime.now().year)
    region = models.CharField(max_length=200)
    alcohol_percentage = models.DecimalField(validators=[MinValueValidator(0.0), MaxValueValidator(100.0)],
                                             max_digits=3, decimal_places=1)
    grapes = models.ManyToManyField(Grape, related_name='wines')
    picture = models.ImageField(upload_to=wine_directory_path)
    visits = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('wines:wine_detail',
                       args=[self.pk])

    def score(self):
        evaluations = self.evaluations.all().aggregate(Avg('score'))
        if evaluations['score__avg'] is not None:
            return round(evaluations['score__avg'], 1)
        else:
            return None

    def __str__(self):
        return self.name


class Evaluation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    wine = models.ForeignKey(Wine, on_delete=models.CASCADE, related_name='evaluations')
    description = models.TextField()
    score = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    created = models.DateField(auto_now_add=True)

    def __str__(self):
        return 'Critic by ' + self.user.username + ' for ' + self.wine.name
