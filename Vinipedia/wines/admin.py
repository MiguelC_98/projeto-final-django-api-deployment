from django.contrib import admin
from wines.models import Wine, Evaluation

admin.site.register(Wine)
admin.site.register(Evaluation)
