from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from wines import views

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('wines/', views.wine_list, name='wine_list'),
    path('wines/<int:pk>', views.wine_detail, name='wine_detail'),
    path('wines/<str:wine_type>', views.wine_list, name='wine_per_type'),
    path('search/', views.wine_search, name='search'),
]
