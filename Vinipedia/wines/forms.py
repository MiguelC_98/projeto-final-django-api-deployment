from django import forms
from wines.models import Evaluation


class SearchForm(forms.Form):
    query = forms.CharField(max_length=30, label='Pesquisa aqui:')


class EvaluationForm(forms.ModelForm):

    class Meta:
        model = Evaluation
        fields = ('description', 'score')

    def clean(self):
        cleaned_data = super().clean()
        score = cleaned_data.get('score')
        if score > 5 or score < 1:
            raise forms.ValidationError('A avaliação precisa de um valor maior do que 1 ou menor do que 5.')
        return cleaned_data
