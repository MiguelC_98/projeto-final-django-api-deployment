from django import template
from wines.forms import SearchForm


register = template.Library()

@register.inclusion_tag('wines/wines/search_bar.html')
def search_bar():
    search_form = SearchForm()
    return {"search_form": search_form}
