from django.db import models
from django.urls import reverse


def grapes_directory_path(instance, filename):
    return 'grapes/{0}/{1}'.format(instance.name, filename)


class Grape(models.Model):
    GRAPE_CHOICES = (
        ('cabernet_sauvignon', 'Cabernet Sauvignon'),
        ('montepulciano', 'Montepulciano'),
        ('chardonnay', 'Chardonnay'),
        ('pinot_noir', 'Pinot Noir'),
        ('malbec', 'Malbec'),
        ('sauvignon_blanc', 'Sauvignon Blanc'),
        ('syrah', 'Syrah'),
        ('zinfandel', 'Zinfandel'),
        ('nebbbiolo', 'Nebbiolo'),
        ('sangiovese', 'Sangiovese'),
        ('pinot_grigio', 'Pinot Grigio'),
        ('riesling', 'Riesling'),
        ('grauburgunder', 'Grauburgunder'),
        ('tempranillo', 'Tempranillo'),
        ('primitivo', 'Primitivo'),
        ('cabernet_franc', 'Cabernet Franc')
    )
    name = models.CharField(max_length=50, choices=GRAPE_CHOICES)
    description = models.TextField()
    picture = models.ImageField(upload_to=grapes_directory_path)
    region = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse('grapes:grape_detail',
                       args=[self.pk])

    def __str__(self):
        return self.name
