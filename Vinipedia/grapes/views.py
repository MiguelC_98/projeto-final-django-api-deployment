from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404
from grapes.models import Grape


def grapes_list(request):

    object_list = Grape.objects.all()
    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')

    try:
        grapes = paginator.page(page)
    except PageNotAnInteger:
        grapes = paginator.page(1)
    except EmptyPage:
        grapes = paginator.page(paginator.num_pages)

    return render(request, "grapes/grapes/list.html", {'grapes': grapes, 'page': page})


def grape_detail(request, pk):
    grape = get_object_or_404(Grape, pk=pk)
    return render(request, 'grapes/grapes/detail.html', {'grape': grape})
