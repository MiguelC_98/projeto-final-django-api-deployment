from django.urls import path
from grapes.views import grapes_list, grape_detail

urlpatterns = [

    path('', grapes_list, name='grapes_list'),
    path('<int:pk>', grape_detail, name='grape_detail'),

]
