from django.contrib.auth.models import User
from django.db import models


def profile_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT /<id>/<filename>
    return 'user/{0}/{1}'.format(instance.user.id, filename)


class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE, related_name='profile')
    picture = models.ImageField(upload_to=profile_directory_path, default='default.jpg')

    def __str__(self):
        return self.user.username
