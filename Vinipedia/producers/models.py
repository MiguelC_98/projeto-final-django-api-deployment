from django.db import models
from django.urls import reverse


class Address(models.Model):
    street_name = models.CharField(max_length=250)
    number = models.IntegerField()
    postal_code = models.CharField(max_length=20)
    location = models.CharField(max_length=50)

    def __str__(self):
        return self.street_name + " " + self.location


class Producer(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    website = models.URLField()

    def get_absolute_url(self):
        return reverse('producers:producer_detail',
                       args=[self.pk])

    def __str__(self):
        return self.name


def producer_directory_path(instance, filename):
    return 'producers/{0}/{1}'.format(instance.producer.name, filename)


class ProducerPicture(models.Model):
    producer = models.ForeignKey(Producer, on_delete=models.CASCADE, related_name='pictures')
    picture = models.ImageField(upload_to=producer_directory_path)

    def __str__(self):
        return 'Picture for producer ' + self.producer.name + ' with the id of ' + str(self.pk)
