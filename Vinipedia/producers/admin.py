from django.contrib import admin
from producers.models import ProducerPicture, Producer, Address

admin.site.register(Producer)
admin.site.register(ProducerPicture)
admin.site.register(Address)
