from django.urls import path

from producers.views import producers_list, producer_detail

urlpatterns = [

    path('', producers_list, name='producers_list'),
    path('<int:pk>', producer_detail, name='producer_detail'),

]
