from producers.models import Producer
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404


def producers_list(request):

    object_list = Producer.objects.all()
    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')

    try:
        producers = paginator.page(page)
    except PageNotAnInteger:
        producers = paginator.page(1)
    except EmptyPage:
        producers = paginator.page(paginator.num_pages)

    return render(request, "producers/producers/list.html", {'producers': producers, 'page': page})


def producer_detail(request, pk):
    producer = get_object_or_404(Producer, pk=pk)
    return render(request, 'producers/producers/detail.html', {'producer': producer})
