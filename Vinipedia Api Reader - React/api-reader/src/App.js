import Navbar from "./components/navbar";
import './App.css';
import WineList from "./components/wine-list";
import { useState } from "react";


function App() {

  const[SearchValue, SetSearchValue] = useState("")

  return (
    <div className="App">
      <Navbar SearchValue={SearchValue} SetSearchValue={SetSearchValue}/>
      <p>This is the API Reader</p>
      <WineList SearchValue={SearchValue} />
    </div>
  );
}

export default App;
