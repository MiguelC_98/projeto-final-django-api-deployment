import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Wine from './wine';
import "./WineList.css"
import { BsArrowUp, BsArrowDown } from 'react-icons/bs';




function WineList(SearchValue) {

    const [wineList, setWineList] = useState(null);
    
    const [nameAsc, SetNameAsc] = useState(false);
    
    const [nameDes, SetNameDes] = useState(false);


    
    useEffect(() => {
        console.log("axios getting")
        if (SearchValue.SearchValue.length === 0) {
            if (nameAsc === false && nameDes === true) {
                axios.get(`https://bettervinipedia.herokuapp.com/api/v1/wines/?ordering=-name`).then(data => setWineList(data.data)).catch(error => console.log(error));
            } else if (nameAsc === true && nameDes === false) {
                axios.get(`https://bettervinipedia.herokuapp.com/api/v1/wines/?ordering=name`).then(data => setWineList(data.data)).catch(error => console.log(error));
            } else {
                axios.get("https://bettervinipedia.herokuapp.com/api/v1/wines/").then(data => setWineList(data.data)).catch(error => console.log(error));
            }
        } else {
            if (nameAsc === false && nameDes === true) {
                axios.get(`https://bettervinipedia.herokuapp.com/api/v1/wines/?ordering=-name&search=${SearchValue.SearchValue}`).then(data => setWineList(data.data)).catch(error => console.log(error));
            } else if (nameAsc === true && nameDes === false) {
                axios.get(`https://bettervinipedia.herokuapp.com/api/v1/wines/?ordering=name&search=${SearchValue.SearchValue}`).then(data => setWineList(data.data)).catch(error => console.log(error));
            } else {
                axios.get(`https://bettervinipedia.herokuapp.com/api/v1/wines/?search=${SearchValue.SearchValue}`).then(data => setWineList(data.data)).catch(error => console.log(error));
            }
        }
        
    }, [SearchValue, nameAsc, nameDes])

    function nameAscClick(){

        if (nameDes) {
            
            SetNameDes(false);
        }
        SetNameAsc(true);   
    }

    const nameDesClick = () => {

        if (nameAsc) {

            SetNameAsc(false);
        }
        SetNameDes(true);
    }

    const clearClick = () => {
        SetNameAsc(false);
        SetNameDes(false);
    }
    

    return (
        <div className="wine-list">
            <h1>Lista de vinhos:</h1>
            <h3>Ordem:</h3>
            <button onClick={nameAscClick}>Name <BsArrowUp/> </button>
            <button onClick={nameDesClick}>Name <BsArrowDown/> </button>
            <button onClick={clearClick}>Clear Ordering</button>
            <div>{wineList ? wineList.map((wine) => { return (<Wine wine={wine} />)}):<p></p>}</div>
        </div>
    )
}

export default WineList;
