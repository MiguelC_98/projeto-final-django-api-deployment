import axios from 'axios';
import React, { useRef } from 'react';

function Wine({wine}) {

    const descRef = useRef(null)
    const scoreRef = useRef(null)
    const userRef = useRef(null)
    const passRef = useRef(null)

    function CommentTest() {
        let url = wine.url +'/evaluations'
        console.log(url)
        axios.post(url, {"description":descRef.current.value, "score":scoreRef.current.value}, {
            auth:{
                username: userRef.current.value,
                password: passRef.current.value
            }
        }).then(res => {
            if (res.statusText === "Created") {
                alert("Comment added! :)");
                descRef.current.value = '';
                scoreRef.current.value = '';
                userRef.current.value = '';
                passRef.current.value = '';
            }
        }).catch(err => {
            if (err.response.status===403) {
                alert(err.response.data.detail);
            } else if (err.response.status===400) {
                alert("Provavelmente tentou inserir um valor fora de 1 a 5, logo deu erro. Se não fez isso, tente de novo.")
            }
        })
    }

    return (
        <div className="wine">
            <h2>{wine.name}</h2>
            <p>{wine.description}</p>
            <img src={wine.picture} alt="wine" />
            <div className="comment-div">
                <h3>Avaliação:</h3>
                <h4>Descrição:</h4>
                <textarea ref={descRef}/>
                <h4>Valor (entre 1 a 5):</h4>
                <input type="number" ref={scoreRef}/>
                <h4>Nome do usuário:</h4>
                <input type="text" ref={userRef}/>
                <h4>Palavra-passe:</h4>
                <input type="password" ref={passRef}/>
                <button onClick={CommentTest}>Comment test</button>
            </div>
            
        </div>
        
    )
}

export default Wine;
