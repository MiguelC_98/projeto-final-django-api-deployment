import React, { useRef } from 'react';
import './Navbar.css';
import { GiGrapes } from 'react-icons/gi';
import { GrSearch } from 'react-icons/gr';

function Navbar({SearchValue, SetSearchValue}) {

    const SearchRef = useRef(null)

    const onClickHandler = (event) => {

        SetSearchValue(SearchRef.current.value)

    }

    return (
        <div className="navbar">
            <div className="title">
                <GiGrapes className='grapes'/> 
                <h2>Vinipedia</h2>
            </div>
            <input type="text" className="Search" ref={SearchRef}></input>
            <button type="submit" className="Search-button" onClick={onClickHandler}><GrSearch/></button>
        </div>
    )
}

export default Navbar;
